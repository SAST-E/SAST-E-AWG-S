EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:SPX3819M5-L-3-3 U1
U 1 1 5F01C163
P 3600 1150
F 0 "U1" H 3600 1492 50  0000 C CNN
F 1 "SPX3819M5-L-3-3" H 3600 1401 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3600 1475 50  0001 C CNN
F 3 "https://www.exar.com/content/document.ashx?id=22106&languageid=1033&type=Datasheet&partnumber=SPX3819&filename=SPX3819.pdf&part=SPX3819" H 3600 1150 50  0001 C CNN
	1    3600 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5F01E3E9
P 2700 1350
F 0 "C1" H 2815 1396 50  0000 L CNN
F 1 "10uf" H 2815 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2738 1200 50  0001 C CNN
F 3 "~" H 2700 1350 50  0001 C CNN
	1    2700 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5F01F478
P 3100 1350
F 0 "C2" H 3215 1396 50  0000 L CNN
F 1 "104" H 3215 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3138 1200 50  0001 C CNN
F 3 "~" H 3100 1350 50  0001 C CNN
	1    3100 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5F01FC91
P 4000 1350
F 0 "C3" H 4115 1396 50  0000 L CNN
F 1 "103" H 4115 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4038 1200 50  0001 C CNN
F 3 "~" H 4000 1350 50  0001 C CNN
	1    4000 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5F020658
P 4400 1350
F 0 "C4" H 4515 1396 50  0000 L CNN
F 1 "10uf" H 4515 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4438 1200 50  0001 C CNN
F 3 "~" H 4400 1350 50  0001 C CNN
	1    4400 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5F02065E
P 4800 1350
F 0 "C5" H 4915 1396 50  0000 L CNN
F 1 "104" H 4915 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4838 1200 50  0001 C CNN
F 3 "~" H 4800 1350 50  0001 C CNN
	1    4800 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1050 3250 1050
Wire Wire Line
	2700 1050 2700 1200
Wire Wire Line
	3100 1200 3100 1050
Connection ~ 3100 1050
Wire Wire Line
	3100 1050 2700 1050
Wire Wire Line
	3300 1150 3250 1150
Wire Wire Line
	3250 1150 3250 1050
Connection ~ 3250 1050
Wire Wire Line
	3250 1050 3100 1050
Wire Wire Line
	3900 1150 4000 1150
Wire Wire Line
	4000 1150 4000 1200
$Comp
L power:GND #PWR02
U 1 1 5F02518A
P 2700 1550
F 0 "#PWR02" H 2700 1300 50  0001 C CNN
F 1 "GND" H 2705 1377 50  0000 C CNN
F 2 "" H 2700 1550 50  0001 C CNN
F 3 "" H 2700 1550 50  0001 C CNN
	1    2700 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F025E54
P 3100 1550
F 0 "#PWR03" H 3100 1300 50  0001 C CNN
F 1 "GND" H 3105 1377 50  0000 C CNN
F 2 "" H 3100 1550 50  0001 C CNN
F 3 "" H 3100 1550 50  0001 C CNN
	1    3100 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5F02619D
P 3600 1550
F 0 "#PWR04" H 3600 1300 50  0001 C CNN
F 1 "GND" H 3605 1377 50  0000 C CNN
F 2 "" H 3600 1550 50  0001 C CNN
F 3 "" H 3600 1550 50  0001 C CNN
	1    3600 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5F0263B6
P 4000 1550
F 0 "#PWR05" H 4000 1300 50  0001 C CNN
F 1 "GND" H 4005 1377 50  0000 C CNN
F 2 "" H 4000 1550 50  0001 C CNN
F 3 "" H 4000 1550 50  0001 C CNN
	1    4000 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5F0264B8
P 4400 1550
F 0 "#PWR06" H 4400 1300 50  0001 C CNN
F 1 "GND" H 4405 1377 50  0000 C CNN
F 2 "" H 4400 1550 50  0001 C CNN
F 3 "" H 4400 1550 50  0001 C CNN
	1    4400 1550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F0266F0
P 4800 1550
F 0 "#PWR08" H 4800 1300 50  0001 C CNN
F 1 "GND" H 4805 1377 50  0000 C CNN
F 2 "" H 4800 1550 50  0001 C CNN
F 3 "" H 4800 1550 50  0001 C CNN
	1    4800 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1550 2700 1500
Wire Wire Line
	3100 1500 3100 1550
Wire Wire Line
	3600 1450 3600 1550
Wire Wire Line
	4000 1500 4000 1550
Wire Wire Line
	4400 1500 4400 1550
Wire Wire Line
	4800 1500 4800 1550
Wire Wire Line
	3900 1050 4400 1050
Wire Wire Line
	4400 1050 4400 1200
Wire Wire Line
	4800 1050 4800 1200
Wire Wire Line
	4400 1050 4800 1050
Connection ~ 4400 1050
$Comp
L power:+3V3 #PWR07
U 1 1 5F027B9C
P 4800 950
F 0 "#PWR07" H 4800 800 50  0001 C CNN
F 1 "+3V3" H 4815 1123 50  0000 C CNN
F 2 "" H 4800 950 50  0001 C CNN
F 3 "" H 4800 950 50  0001 C CNN
	1    4800 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1050 4800 950 
Connection ~ 4800 1050
$Comp
L Connector:USB_C_Receptacle_USB2.0 J1
U 1 1 5F0A8653
P 1050 1450
F 0 "J1" H 1157 2317 50  0000 C CNN
F 1 "USB_C_Receptacle_USB2.0" H 1157 2226 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_HRO_TYPE-C-31-M-12" H 1200 1450 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1200 1450 50  0001 C CNN
	1    1050 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5F0AA23B
P 1850 850
F 0 "F1" V 1653 850 50  0000 C CNN
F 1 "Fuse" V 1744 850 50  0000 C CNN
F 2 "Fuse:Fuse_0805_2012Metric" V 1780 850 50  0001 C CNN
F 3 "~" H 1850 850 50  0001 C CNN
	1    1850 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 850  1650 850 
NoConn ~ 1650 1050
NoConn ~ 1650 1150
NoConn ~ 1650 1950
NoConn ~ 1650 2050
$Comp
L power:GND #PWR09
U 1 1 5F0ABFEE
P 1050 2400
F 0 "#PWR09" H 1050 2150 50  0001 C CNN
F 1 "GND" H 1050 2250 50  0000 C CNN
F 2 "" H 1050 2400 50  0001 C CNN
F 3 "" H 1050 2400 50  0001 C CNN
	1    1050 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5F0AC828
P 750 2400
F 0 "#PWR01" H 750 2150 50  0001 C CNN
F 1 "GND" H 750 2250 50  0000 C CNN
F 2 "" H 750 2400 50  0001 C CNN
F 3 "" H 750 2400 50  0001 C CNN
	1    750  2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  2400 750  2350
Wire Wire Line
	1050 2350 1050 2400
Text GLabel 1700 1400 2    50   Input ~ 0
USB_N
Text GLabel 1700 1600 2    50   Input ~ 0
USB_P
Wire Wire Line
	1700 1600 1650 1600
Wire Wire Line
	1650 1600 1650 1550
Wire Wire Line
	1650 1600 1650 1650
Connection ~ 1650 1600
Wire Wire Line
	1700 1400 1650 1400
Wire Wire Line
	1650 1400 1650 1350
Wire Wire Line
	1650 1400 1650 1450
Connection ~ 1650 1400
$Comp
L power:+3V3 #PWR010
U 1 1 5F0B9850
P 3450 3050
F 0 "#PWR010" H 3450 2900 50  0001 C CNN
F 1 "+3V3" H 3465 3223 50  0000 C CNN
F 2 "" H 3450 3050 50  0001 C CNN
F 3 "" H 3450 3050 50  0001 C CNN
	1    3450 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3050 3450 3150
Wire Wire Line
	3250 3150 3350 3150
Connection ~ 3350 3150
Wire Wire Line
	3350 3150 3450 3150
Connection ~ 3450 3150
Wire Wire Line
	3450 3150 3550 3150
Connection ~ 3550 3150
Wire Wire Line
	3550 3150 3650 3150
$Comp
L Device:C C6
U 1 1 5F0BAF8B
P 4150 3200
F 0 "C6" H 4265 3246 50  0000 L CNN
F 1 "10uf" H 4265 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4188 3050 50  0001 C CNN
F 3 "~" H 4150 3200 50  0001 C CNN
	1    4150 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5F0BB8A0
P 4550 3200
F 0 "C7" H 4665 3246 50  0000 L CNN
F 1 "104" H 4665 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4588 3050 50  0001 C CNN
F 3 "~" H 4550 3200 50  0001 C CNN
	1    4550 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5F0BC5F1
P 4950 3200
F 0 "C8" H 5065 3246 50  0000 L CNN
F 1 "104" H 5065 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4988 3050 50  0001 C CNN
F 3 "~" H 4950 3200 50  0001 C CNN
	1    4950 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5F0BCD19
P 5350 3200
F 0 "C9" H 5465 3246 50  0000 L CNN
F 1 "104" H 5465 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5388 3050 50  0001 C CNN
F 3 "~" H 5350 3200 50  0001 C CNN
	1    5350 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5F0BD2C5
P 5700 3200
F 0 "C10" H 5815 3246 50  0000 L CNN
F 1 "104" H 5815 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5738 3050 50  0001 C CNN
F 3 "~" H 5700 3200 50  0001 C CNN
	1    5700 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3050 5350 3050
Connection ~ 3450 3050
Connection ~ 4150 3050
Wire Wire Line
	4150 3050 3450 3050
Connection ~ 4550 3050
Wire Wire Line
	4550 3050 4150 3050
Connection ~ 4950 3050
Wire Wire Line
	4950 3050 4550 3050
Connection ~ 5350 3050
Wire Wire Line
	5350 3050 4950 3050
$Comp
L power:GND #PWR011
U 1 1 5F0BECD1
P 4150 3400
F 0 "#PWR011" H 4150 3150 50  0001 C CNN
F 1 "GND" H 4150 3250 50  0000 C CNN
F 2 "" H 4150 3400 50  0001 C CNN
F 3 "" H 4150 3400 50  0001 C CNN
	1    4150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3350 4150 3400
$Comp
L power:GND #PWR012
U 1 1 5F0BF5C0
P 4550 3400
F 0 "#PWR012" H 4550 3150 50  0001 C CNN
F 1 "GND" H 4550 3250 50  0000 C CNN
F 2 "" H 4550 3400 50  0001 C CNN
F 3 "" H 4550 3400 50  0001 C CNN
	1    4550 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3350 4550 3400
$Comp
L power:GND #PWR013
U 1 1 5F0BFE61
P 4950 3400
F 0 "#PWR013" H 4950 3150 50  0001 C CNN
F 1 "GND" H 4950 3250 50  0000 C CNN
F 2 "" H 4950 3400 50  0001 C CNN
F 3 "" H 4950 3400 50  0001 C CNN
	1    4950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3350 4950 3400
$Comp
L power:GND #PWR014
U 1 1 5F0C0633
P 5350 3400
F 0 "#PWR014" H 5350 3150 50  0001 C CNN
F 1 "GND" H 5350 3250 50  0000 C CNN
F 2 "" H 5350 3400 50  0001 C CNN
F 3 "" H 5350 3400 50  0001 C CNN
	1    5350 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3350 5350 3400
$Comp
L power:GND #PWR015
U 1 1 5F0C1015
P 5700 3400
F 0 "#PWR015" H 5700 3150 50  0001 C CNN
F 1 "GND" H 5700 3250 50  0000 C CNN
F 2 "" H 5700 3400 50  0001 C CNN
F 3 "" H 5700 3400 50  0001 C CNN
	1    5700 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3350 5700 3400
$Comp
L power:GND #PWR017
U 1 1 5F0C1FA2
P 3400 6200
F 0 "#PWR017" H 3400 5950 50  0001 C CNN
F 1 "GND" H 3400 6050 50  0000 C CNN
F 2 "" H 3400 6200 50  0001 C CNN
F 3 "" H 3400 6200 50  0001 C CNN
	1    3400 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6150 3250 6200
Wire Wire Line
	3350 6150 3350 6200
Wire Wire Line
	3450 6150 3450 6200
Wire Wire Line
	3550 6150 3550 6200
Wire Wire Line
	3550 6200 3450 6200
Connection ~ 3350 6200
Wire Wire Line
	3350 6200 3250 6200
Connection ~ 3450 6200
Wire Wire Line
	3450 6200 3400 6200
Connection ~ 3400 6200
Wire Wire Line
	3400 6200 3350 6200
Text Label 2550 4450 0    50   ~ 0
PB0
Wire Wire Line
	2550 4450 2750 4450
Text Label 2550 4550 0    50   ~ 0
PB1
Wire Wire Line
	2550 4550 2750 4550
Text Label 2550 4650 0    50   ~ 0
PB2
Wire Wire Line
	2550 4650 2750 4650
Text Label 2550 4750 0    50   ~ 0
PB3
Wire Wire Line
	2550 4750 2750 4750
Text Label 2550 4850 0    50   ~ 0
PB4
Wire Wire Line
	2550 4850 2750 4850
Text Label 2550 4950 0    50   ~ 0
PB5
Wire Wire Line
	2550 4950 2750 4950
Text Label 2550 5050 0    50   ~ 0
PB6
Wire Wire Line
	2550 5050 2750 5050
Text Label 2550 5150 0    50   ~ 0
PB7
Wire Wire Line
	2550 5150 2750 5150
Text Label 2550 5250 0    50   ~ 0
PB8
Wire Wire Line
	2550 5250 2750 5250
Text Label 2550 5350 0    50   ~ 0
PB9
Wire Wire Line
	2550 5350 2750 5350
Text Label 2550 5450 0    50   ~ 0
PB10
Wire Wire Line
	2550 5450 2750 5450
Text Label 2550 5550 0    50   ~ 0
PB11
Wire Wire Line
	2550 5550 2750 5550
Text Label 2550 5650 0    50   ~ 0
PB12
Wire Wire Line
	2550 5650 2750 5650
Text Label 2550 5750 0    50   ~ 0
PB13
Wire Wire Line
	2550 5750 2750 5750
$Comp
L Device:C C11
U 1 1 5F0D607A
P 2550 3350
F 0 "C11" V 2298 3350 50  0000 C CNN
F 1 "104" V 2389 3350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2588 3200 50  0001 C CNN
F 3 "~" H 2550 3350 50  0001 C CNN
	1    2550 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 3350 2750 3350
$Comp
L power:GND #PWR016
U 1 1 5F0D88EF
P 2350 3350
F 0 "#PWR016" H 2350 3100 50  0001 C CNN
F 1 "GND" V 2355 3222 50  0000 R CNN
F 2 "" H 2350 3350 50  0001 C CNN
F 3 "" H 2350 3350 50  0001 C CNN
	1    2350 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 3350 2400 3350
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J2
U 1 1 5F0DAABC
P 1200 4900
F 0 "J2" H 1250 5517 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 1250 5426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x10_P2.54mm_Horizontal" H 1200 4900 50  0001 C CNN
F 3 "~" H 1200 4900 50  0001 C CNN
	1    1200 4900
	1    0    0    -1  
$EndComp
Text Label 1500 5100 0    50   ~ 0
PB0
Text Label 800  5100 0    50   ~ 0
PB1
Wire Wire Line
	800  5100 1000 5100
Text Label 1500 5000 0    50   ~ 0
PB2
Text Label 800  5000 0    50   ~ 0
PB3
Wire Wire Line
	800  5000 1000 5000
Text Label 1500 4900 0    50   ~ 0
PB4
Text Label 800  4900 0    50   ~ 0
PB5
Wire Wire Line
	800  4900 1000 4900
Text Label 1500 4800 0    50   ~ 0
PB6
Text Label 800  4800 0    50   ~ 0
PB7
Wire Wire Line
	800  4800 1000 4800
Text Label 1500 4700 0    50   ~ 0
PB8
Text Label 800  4700 0    50   ~ 0
PB9
Wire Wire Line
	800  4700 1000 4700
Text Label 1500 4600 0    50   ~ 0
PB10
Text Label 800  4600 0    50   ~ 0
PB11
Wire Wire Line
	800  4600 1000 4600
Text Label 1500 4500 0    50   ~ 0
PB12
Text Label 800  4500 0    50   ~ 0
PB13
Wire Wire Line
	800  4500 1000 4500
Text GLabel 1000 5200 0    50   Input ~ 0
DA_CLK
Text GLabel 1000 5300 0    50   Input ~ 0
SCLK
$Comp
L power:GND #PWR018
U 1 1 5F0F3ED5
P 1000 5400
F 0 "#PWR018" H 1000 5150 50  0001 C CNN
F 1 "GND" V 1005 5272 50  0000 R CNN
F 2 "" H 1000 5400 50  0001 C CNN
F 3 "" H 1000 5400 50  0001 C CNN
	1    1000 5400
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR021
U 1 1 5F0F472F
P 2700 950
F 0 "#PWR021" H 2700 800 50  0001 C CNN
F 1 "+5V" H 2715 1123 50  0000 C CNN
F 2 "" H 2700 950 50  0001 C CNN
F 3 "" H 2700 950 50  0001 C CNN
	1    2700 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 950  2700 1050
Connection ~ 2700 1050
$Comp
L power:+5V #PWR020
U 1 1 5F0FCAB4
P 2100 800
F 0 "#PWR020" H 2100 650 50  0001 C CNN
F 1 "+5V" H 2115 973 50  0000 C CNN
F 2 "" H 2100 800 50  0001 C CNN
F 3 "" H 2100 800 50  0001 C CNN
	1    2100 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 800  2100 850 
Wire Wire Line
	2100 850  2000 850 
$Comp
L power:+5V #PWR019
U 1 1 5F0FF46C
P 1500 5400
F 0 "#PWR019" H 1500 5250 50  0001 C CNN
F 1 "+5V" V 1500 5500 50  0000 L CNN
F 2 "" H 1500 5400 50  0001 C CNN
F 3 "" H 1500 5400 50  0001 C CNN
	1    1500 5400
	0    1    1    0   
$EndComp
Text GLabel 1500 5200 2    50   Input ~ 0
DIN
Text GLabel 1500 5300 2    50   Input ~ 0
SYNC
Entry Wire Line
	2450 5550 2550 5650
Entry Wire Line
	2450 5450 2550 5550
Entry Wire Line
	2450 5350 2550 5450
Entry Wire Line
	2450 5650 2550 5750
Entry Wire Line
	2450 5250 2550 5350
Entry Wire Line
	2450 5150 2550 5250
Entry Wire Line
	2450 5050 2550 5150
Entry Wire Line
	2450 4950 2550 5050
Entry Wire Line
	2450 4850 2550 4950
Entry Wire Line
	2450 4750 2550 4850
Entry Wire Line
	2450 4650 2550 4750
Entry Wire Line
	2450 4550 2550 4650
Entry Wire Line
	2450 4450 2550 4550
Entry Wire Line
	2450 4350 2550 4450
Entry Wire Line
	700  5000 800  5100
Entry Wire Line
	700  4900 800  5000
Entry Wire Line
	700  4800 800  4900
Entry Wire Line
	700  4700 800  4800
Entry Wire Line
	700  4600 800  4700
Entry Wire Line
	700  4500 800  4600
Entry Wire Line
	700  4400 800  4500
Entry Wire Line
	1800 5000 1700 5100
Entry Wire Line
	1800 4900 1700 5000
Entry Wire Line
	1800 4800 1700 4900
Entry Wire Line
	1800 4700 1700 4800
Entry Wire Line
	1800 4600 1700 4700
Entry Wire Line
	1800 4500 1700 4600
Entry Wire Line
	1800 4400 1700 4500
Wire Wire Line
	1700 4500 1500 4500
Wire Wire Line
	1500 4600 1700 4600
Wire Wire Line
	1500 4700 1700 4700
Wire Wire Line
	1500 4800 1700 4800
Wire Wire Line
	1500 4900 1700 4900
Wire Wire Line
	1500 5000 1700 5000
Wire Wire Line
	1500 5100 1700 5100
Wire Bus Line
	700  4200 1800 4200
Connection ~ 1800 4200
Wire Bus Line
	1800 4200 2450 4200
Text Label 1950 4200 0    50   ~ 0
PB[0..13]
$Comp
L Device:Crystal Y1
U 1 1 5F123797
P 2300 3800
F 0 "Y1" V 2254 3669 50  0000 R CNN
F 1 "Crystal" V 2345 3669 50  0000 R CNN
F 2 "Crystal:Crystal_SMD_5032-2Pin_5.0x3.2mm" H 2300 3800 50  0001 C CNN
F 3 "~" H 2300 3800 50  0001 C CNN
	1    2300 3800
	0    -1   1    0   
$EndComp
$Comp
L Device:C C12
U 1 1 5F125022
P 2000 3650
F 0 "C12" V 2050 3500 50  0000 C CNN
F 1 "20pf" V 2050 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2038 3500 50  0001 C CNN
F 3 "~" H 2000 3650 50  0001 C CNN
	1    2000 3650
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 5F1267E9
P 2000 3950
F 0 "C13" V 2050 4100 50  0000 C CNN
F 1 "20pf" V 2050 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2038 3800 50  0001 C CNN
F 3 "~" H 2000 3950 50  0001 C CNN
	1    2000 3950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5F12C16F
P 1800 3650
F 0 "#PWR022" H 1800 3400 50  0001 C CNN
F 1 "GND" V 1805 3522 50  0000 R CNN
F 2 "" H 1800 3650 50  0001 C CNN
F 3 "" H 1800 3650 50  0001 C CNN
	1    1800 3650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5F12D014
P 1800 3950
F 0 "#PWR023" H 1800 3700 50  0001 C CNN
F 1 "GND" V 1805 3822 50  0000 R CNN
F 2 "" H 1800 3950 50  0001 C CNN
F 3 "" H 1800 3950 50  0001 C CNN
	1    1800 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 3950 1850 3950
Wire Wire Line
	1800 3650 1850 3650
Wire Wire Line
	2150 3650 2300 3650
Wire Wire Line
	2150 3950 2300 3950
Connection ~ 2300 3650
Wire Wire Line
	2750 3850 2700 3850
Wire Wire Line
	2700 3850 2700 3950
Wire Wire Line
	2700 3950 2300 3950
Connection ~ 2300 3950
Wire Wire Line
	2700 3650 2700 3750
Wire Wire Line
	2700 3750 2750 3750
Wire Wire Line
	2300 3650 2700 3650
$Comp
L Device:R R1
U 1 1 5F14F1B3
P 2600 3550
F 0 "R1" V 2550 3350 50  0000 C CNN
F 1 "10K" V 2600 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2530 3550 50  0001 C CNN
F 3 "~" H 2600 3550 50  0001 C CNN
	1    2600 3550
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5F1502CD
P 2400 3550
F 0 "#PWR024" H 2400 3300 50  0001 C CNN
F 1 "GND" V 2400 3450 50  0000 R CNN
F 2 "" H 2400 3550 50  0001 C CNN
F 3 "" H 2400 3550 50  0001 C CNN
	1    2400 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	2400 3550 2450 3550
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U2
U 1 1 5F0B792B
P 3450 4650
F 0 "U2" H 4300 5450 50  0000 C CNN
F 1 "STM32F103C8Tx" H 4300 5350 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 2850 3250 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 3450 4650 50  0001 C CNN
	1    3450 4650
	1    0    0    -1  
$EndComp
NoConn ~ 2750 4150
NoConn ~ 2750 4250
NoConn ~ 2750 5850
NoConn ~ 2750 5950
Text GLabel 4050 5350 2    50   Input ~ 0
MCU_TX
Text GLabel 4050 5450 2    50   Input ~ 0
MCU_RX
Text GLabel 4050 5750 2    50   Input ~ 0
SW_DIO
Text GLabel 4050 5850 2    50   Input ~ 0
SW_CLK
NoConn ~ 4050 5950
NoConn ~ 4050 5650
NoConn ~ 4050 5550
Text GLabel 4050 4850 2    50   Input ~ 0
SYNC
Text GLabel 4050 4950 2    50   Input ~ 0
SCLK
Text GLabel 4050 5150 2    50   Input ~ 0
DIN
Text GLabel 4050 5050 2    50   Input ~ 0
DA_CLK
NoConn ~ 4050 5250
Text GLabel 4050 4750 2    50   Input ~ 0
PWM_OUT
NoConn ~ 4050 4450
NoConn ~ 4050 4550
NoConn ~ 4050 4650
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5F17D790
P 4950 4200
F 0 "J3" H 4868 4517 50  0000 C CNN
F 1 "Conn_01x04" H 4868 4426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4950 4200 50  0001 C CNN
F 3 "~" H 4950 4200 50  0001 C CNN
	1    4950 4200
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR025
U 1 1 5F181A60
P 5150 4100
F 0 "#PWR025" H 5150 3950 50  0001 C CNN
F 1 "+3V3" V 5150 4200 50  0000 L CNN
F 2 "" H 5150 4100 50  0001 C CNN
F 3 "" H 5150 4100 50  0001 C CNN
	1    5150 4100
	0    1    1    0   
$EndComp
Text GLabel 5150 4300 2    50   Input ~ 0
SW_DIO
Text GLabel 5150 4200 2    50   Input ~ 0
SW_CLK
$Comp
L power:GND #PWR026
U 1 1 5F18558A
P 5150 4400
F 0 "#PWR026" H 5150 4150 50  0001 C CNN
F 1 "GND" V 5150 4300 50  0000 R CNN
F 2 "" H 5150 4400 50  0001 C CNN
F 3 "" H 5150 4400 50  0001 C CNN
	1    5150 4400
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_Coaxial J4
U 1 1 5F18B20E
P 5900 5050
F 0 "J4" H 6000 5025 50  0000 L CNN
F 1 "Conn_Coaxial" H 6000 4934 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Samtec_SMA-J-P-X-ST-EM1_EdgeMount" H 5900 5050 50  0001 C CNN
F 3 " ~" H 5900 5050 50  0001 C CNN
	1    5900 5050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5F18BC51
P 5900 5250
F 0 "#PWR027" H 5900 5000 50  0001 C CNN
F 1 "GND" H 5905 5077 50  0000 C CNN
F 2 "" H 5900 5250 50  0001 C CNN
F 3 "" H 5900 5250 50  0001 C CNN
	1    5900 5250
	1    0    0    -1  
$EndComp
Text GLabel 5100 5050 0    50   Input ~ 0
PWM_OUT
$Comp
L Interface_USB:CH340E U3
U 1 1 5F036CF4
P 6350 1550
F 0 "U3" H 6550 2250 50  0000 C CNN
F 1 "CH340E" H 6550 2150 50  0000 C CNN
F 2 "Package_SO:MSOP-10_3x3mm_P0.5mm" H 6400 1000 50  0001 L CNN
F 3 "https://www.mpja.com/download/35227cpdata.pdf" H 6000 2350 50  0001 C CNN
	1    6350 1550
	1    0    0    -1  
$EndComp
NoConn ~ 6750 1450
NoConn ~ 6750 1950
$Comp
L power:GND #PWR030
U 1 1 5F03E5BA
P 6350 2200
F 0 "#PWR030" H 6350 1950 50  0001 C CNN
F 1 "GND" H 6355 2027 50  0000 C CNN
F 2 "" H 6350 2200 50  0001 C CNN
F 3 "" H 6350 2200 50  0001 C CNN
	1    6350 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2200 6350 2150
$Comp
L power:+3V3 #PWR029
U 1 1 5F04260F
P 6350 900
F 0 "#PWR029" H 6350 750 50  0001 C CNN
F 1 "+3V3" H 6365 1073 50  0000 C CNN
F 2 "" H 6350 900 50  0001 C CNN
F 3 "" H 6350 900 50  0001 C CNN
	1    6350 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 900  6350 950 
Wire Wire Line
	6350 950  6250 950 
Connection ~ 6350 950 
$Comp
L Device:C C14
U 1 1 5F049A7D
P 6050 900
F 0 "C14" V 5798 900 50  0000 C CNN
F 1 "104" V 5889 900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6088 750 50  0001 C CNN
F 3 "~" H 6050 900 50  0001 C CNN
	1    6050 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 900  6350 900 
Connection ~ 6350 900 
$Comp
L power:GND #PWR028
U 1 1 5F04ED25
P 5850 900
F 0 "#PWR028" H 5850 650 50  0001 C CNN
F 1 "GND" V 5850 800 50  0000 R CNN
F 2 "" H 5850 900 50  0001 C CNN
F 3 "" H 5850 900 50  0001 C CNN
	1    5850 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 900  5900 900 
NoConn ~ 5950 1650
Text GLabel 5950 1450 0    50   Input ~ 0
USB_P
Text GLabel 5950 1550 0    50   Input ~ 0
USB_N
Text GLabel 6750 1250 2    50   Input ~ 0
MCU_TX
Text GLabel 6750 1150 2    50   Input ~ 0
MCU_RX
Text Label 2550 3650 2    50   ~ 0
Cry_P
Text Label 2550 3950 2    50   ~ 0
Cry_N
Text GLabel 2750 4050 0    50   Input ~ 0
LED-1
Text GLabel 5000 6050 0    50   Input ~ 0
LED-1
$Comp
L Device:LED D1
U 1 1 5F0B183A
P 5200 6050
F 0 "D1" H 5193 6267 50  0000 C CNN
F 1 "LED" H 5193 6176 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5200 6050 50  0001 C CNN
F 3 "~" H 5200 6050 50  0001 C CNN
	1    5200 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 6050 5000 6050
$Comp
L Device:R R2
U 1 1 5F0B5B1E
P 5550 6050
F 0 "R2" V 5343 6050 50  0000 C CNN
F 1 "3K" V 5434 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5480 6050 50  0001 C CNN
F 3 "~" H 5550 6050 50  0001 C CNN
	1    5550 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 6050 5350 6050
$Comp
L power:+3V3 #PWR031
U 1 1 5F0BA2B0
P 5750 6050
F 0 "#PWR031" H 5750 5900 50  0001 C CNN
F 1 "+3V3" V 5765 6178 50  0000 L CNN
F 2 "" H 5750 6050 50  0001 C CNN
F 3 "" H 5750 6050 50  0001 C CNN
	1    5750 6050
	0    1    1    0   
$EndComp
Wire Wire Line
	5750 6050 5700 6050
$Comp
L Diode:1N4148W D2
U 1 1 5F0C5AFB
P 5300 4900
F 0 "D2" V 5250 4650 50  0000 L CNN
F 1 "1N4148W" V 5350 4450 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 5300 4725 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" H 5300 4900 50  0001 C CNN
	1    5300 4900
	0    1    1    0   
$EndComp
$Comp
L Diode:1N4148W D3
U 1 1 5F0C7251
P 5300 5200
F 0 "D3" V 5250 4950 50  0000 L CNN
F 1 "1N4148W" V 5350 4750 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 5300 5025 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85748/1n4148w.pdf" H 5300 5200 50  0001 C CNN
	1    5300 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5F0CAA23
P 5500 5050
F 0 "R3" V 5293 5050 50  0000 C CNN
F 1 "510R" V 5384 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5430 5050 50  0001 C CNN
F 3 "~" H 5500 5050 50  0001 C CNN
	1    5500 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 5050 5650 5050
Wire Wire Line
	5350 5050 5300 5050
Connection ~ 5300 5050
$Comp
L power:GND #PWR033
U 1 1 5F0DCB7B
P 5300 5350
F 0 "#PWR033" H 5300 5100 50  0001 C CNN
F 1 "GND" H 5305 5177 50  0000 C CNN
F 2 "" H 5300 5350 50  0001 C CNN
F 3 "" H 5300 5350 50  0001 C CNN
	1    5300 5350
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR032
U 1 1 5F0DD2D3
P 5300 4750
F 0 "#PWR032" H 5300 4600 50  0001 C CNN
F 1 "+3V3" H 5315 4923 50  0000 C CNN
F 2 "" H 5300 4750 50  0001 C CNN
F 3 "" H 5300 4750 50  0001 C CNN
	1    5300 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 5050 5300 5050
Wire Bus Line
	700  4200 700  5000
Wire Bus Line
	1800 4200 1800 5000
Wire Bus Line
	2450 4200 2450 5650
$EndSCHEMATC
