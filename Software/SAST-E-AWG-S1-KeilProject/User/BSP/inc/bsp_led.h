#ifndef __BSP_LED_H
#define __BSP_LED_H

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "bsp_timer.h"

//根据核心板类型选择板载LED的端口和引脚，不用的信息请注释掉

//#define BLACK_PILL	0 //黑色34Pin的C8T6核心板
#define BLUE_PILL	0 //蓝色40Pin的C8T6核心板

#ifdef BLUE_PILL

#define BOARD_LED_PORT GPIOC
#define BOARD_LED_PIN  GPIO_Pin_13

#elif defined BLACK_PILL  

#define BOARD_LED_PORT GPIOB
#define BOARD_LED_PIN  GPIO_Pin_12

#endif

void Board_LED_Init(void);

void Board_LED_ON(void);
void Board_LED_OFF(void);

void Board_LED_Blink(uint8_t times,uint16_t delay_time_ms);

#endif


