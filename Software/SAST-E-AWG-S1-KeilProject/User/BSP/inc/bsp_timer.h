#ifndef __BSP_TIMER_H
#define __BSP_TIMER_H

#include "stm32f10x.h"
#include "stm32f10x_conf.h"

void Bsp_Delay_Init(void);
void Bsp_Delay_us(uint32_t _time);
void Bsp_Delay_ms(uint16_t _time);


#endif 

