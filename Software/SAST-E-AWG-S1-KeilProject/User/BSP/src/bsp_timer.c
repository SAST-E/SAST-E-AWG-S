#include "bsp_timer.h"


void Bsp_Delay_Init(void)
{
	//设置Systick定时器重装载值，并使能Systick中断，注意Systick为自减的定时器
	//当主频为72MHz时，LOAD寄存器的值被设置为72000-1
	//当主频为72MHz时、AHB预分频器不分频时计数频率为72MHz
	SysTick_Config(SystemCoreClock/1000);

	
}

inline void Bsp_Delay_us(uint32_t _time)
{
	//计数频率72MHz时，72次即为1us，为了避免在输入0使出现错误，计数值没有-1。
	//这会导致一些误差，但可以接受，即使是_time为1时这一误差也不超过2%
	SysTick->LOAD = 72 * _time;
	
	//请自行查看CTRL寄存器的相关说明，这里的操作只是将Bit0、Bit2置1，而其余有效Bit（1和16）和保留Bit均置零
	//即 允许SysTick计数、不触发中断、计数频率为AHB频率
	SysTick->CTRL = SysTick_CTRL_ENABLE_Msk | SysTick_CTRL_CLKSOURCE_Msk;
	
	//等待SysTick自减至零以后，关闭SysTick
	while(!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk));
	
	SysTick->CTRL = ~SysTick_CTRL_ENABLE_Msk;
	
}

inline void Bsp_Delay_ms(uint16_t _time)
{
	//请注意，由于这里采用了do while的结构，最小延时为1mS
	uint16_t time = _time;
	do {
		Bsp_Delay_us(1000);
	}while(--time);
	
}
