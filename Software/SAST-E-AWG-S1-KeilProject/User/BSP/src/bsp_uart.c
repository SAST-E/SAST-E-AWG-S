#include "bsp_uart.h"

Birch_UART_T g_UartDevice[] = {
	{.USARTx = USART1,.RCC_Periph_Base = RCC_APB2Periph_USART1,
	 .TX_GPIOx = GPIOA,.TX_Pin = GPIO_Pin_9,
	 .RX_GPIOx = GPIOA,.RX_Pin = GPIO_Pin_10,
	 .baud = 115200,   .IRQChannel = USART1_IRQn},
	
};

//把uart放到bsp这里主要是因为准备做几个函数和板子上的ESP-01S模块交互用
uint8_t usart1_rx_buffer[400];
uint8_t usart1_rx_ok = 0;//接收完成标识
uint8_t usart1_rx_flag = 0;//标志数据接收状态标识，最低位为1表示收到了0x0D
uint16_t usart1_rx_count = 0;

void Uart_Init(Birch_UART_T *pUART)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(pUART->RCC_Periph_Base, ENABLE);	//ENABLE Clock
	
	RCC_APB2PeriphClockCmd((uint32_t)(1<<(((uint32_t)pUART->TX_GPIOx - APB2PERIPH_BASE)>>10)) \
	,ENABLE); //打开端口时钟,
	RCC_APB2PeriphClockCmd((uint32_t)(1<<(((uint32_t)pUART->RX_GPIOx - APB2PERIPH_BASE)>>10)) \
	,ENABLE); //打开端口时钟
	

	GPIO_InitStructure.GPIO_Pin = pUART->TX_Pin; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
	GPIO_Init(pUART->TX_GPIOx, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = pUART->RX_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(pUART->RX_GPIOx, &GPIO_InitStructure);

	USART_InitStructure.USART_BaudRate = pUART->baud;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	
	
	NVIC_InitStructure.NVIC_IRQChannel = pUART->IRQChannel;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			
	NVIC_Init(&NVIC_InitStructure);

	USART_Init(pUART->USARTx, &USART_InitStructure); 
	USART_ITConfig(pUART->USARTx, USART_IT_RXNE, ENABLE);
	USART_Cmd(pUART->USARTx, ENABLE);                    

}

void USART1_IRQHandler(void)                	
{
	uint8_t ch;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)  
	{
		ch = USART_ReceiveData(USART1);	//从接收缓冲区读取一字节数据
		if(0 == usart1_rx_ok)//接收未完成
		{
			if(0x01 & usart1_rx_flag)//接收到了0x0d
			{
				if(ch != 0x0a) {//接收错误，重新开始
					usart1_rx_flag = 0; 
					usart1_rx_count = 0;
				}
				else {
					usart1_rx_ok = 1;	//接收完成
					//usart1_rx_count = 0;//如需使用count请注释掉这句
					
					usart1_rx_flag = 0;
				}
			}
			else //未收到结束标志
			{	
				if(0x0d == ch) {
					usart1_rx_flag |= 0x01;
				}
				else {
					usart1_rx_buffer[usart1_rx_count] = ch;
					usart1_rx_count++;
					if(usart1_rx_count > 400) {//数据错误,重新开始
						usart1_rx_count = 0;
						usart1_rx_flag  = 0;
					}
				}		 
			}
		}   		 
	 } 
} 

int fputc(int ch, FILE *f)
{
   USART_SendData(USART1, (uint8_t)ch);
   while(!(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == SET)) {
   }
   
   return ch;
}


