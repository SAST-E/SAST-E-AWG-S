/*
 * @Descripttion: 以主机模式进行IIC通信的代码，可以管理多条模拟IIC总线并为不同总线指定不同的速率
 * @version: V0.2
 * @Author: Birch
 * @Date: 2020-03-18 16:02:42
 * @LastEditors: Birch
 * @LastEditTime: 2020-03-21 16:41:31
 */
#include "birch_iic.h"

//为不同的IIC总线指定端口、引脚和速率等级
Birch_IIC_Bus_T birch_iic_bus[] = {
	[0] = 	{.SCL_GPIOx = GPIOB,.SCL_Pin = GPIO_Pin_5, 
			 .SDA_GPIOx = GPIOB,.SDA_Pin = GPIO_Pin_6,
			 .Speed_range = 2},
	
	[1] = 	{.SCL_GPIOx = GPIOB,.SCL_Pin = GPIO_Pin_8, 
			 .SDA_GPIOx = GPIOB,.SDA_Pin = GPIO_Pin_9,
			 .Speed_range = 0},
	
	[2] = 	{.SCL_GPIOx = GPIOA,.SCL_Pin = GPIO_Pin_5, 
			 .SDA_GPIOx = GPIOA,.SDA_Pin = GPIO_Pin_6,
			 .Speed_range = 2}
};

/**
 * @brief: IIC通信中的延时，速率等级越低延时时间越短
 * @example: Birch_IIC_Delay(bus);
 * @param:   bus为指定的IIC总线
 * @return:  None 
 */
inline void Birch_IIC_Delay(Birch_IIC_Bus_T *bus)
{
	if(0 == bus->Speed_range){
		__nop();__nop();__nop();
	}
	else if(1 == bus->Speed_range){
		Bsp_Delay_us(2);
	}
	else{
		Bsp_Delay_us(4);
	}
}

/**
 * @brief: Initial pins for IIC communications.
 * @example: Birch_IIC_Pin_Init(BIRCH_IIC_SCL_PORT,BIRCH_IIC_SCL_PIN);
 * @param:   GPIOx为端口号，Pin为引脚号
 * @return:  None
 */
void Birch_IIC_Pin_Init(GPIO_TypeDef *GPIOx,uint16_t Pin)
{
	GPIO_InitTypeDef GPIO_InitStructer;

	RCC_APB2PeriphClockCmd((uint32_t)(1<<(((uint32_t)GPIOx - APB2PERIPH_BASE)>>10)) \
	,ENABLE);

	GPIO_InitStructer.GPIO_Pin = Pin; 
	GPIO_InitStructer.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructer.GPIO_Mode = GPIO_Mode_Out_PP;

	GPIO_Init(GPIOx, &GPIO_InitStructer);
}

/**
 * @brief: 将引脚置为高电平
 * @example: None
 * @param:   GPIOx为端口号，Pin为引脚号
 * @return:  None 
 */
void Birch_IIC_Set_Pin(GPIO_TypeDef *GPIOx,uint16_t Pin)
{
	GPIOx->BSRR = (uint32_t)Pin;
}

/**
 * @brief: 将引脚置为低电平
 * @example: None
 * @param:   GPIOx为端口号，Pin为引脚号
 * @return:  None
 */
void Birch_IIC_Reset_Pin(GPIO_TypeDef *GPIOx,uint16_t Pin)
{
	GPIOx->BRR = (uint32_t)Pin;
}

/**
 * @brief: 初始化指定的IIC总线所占用的引脚并将相应引脚置为高电平
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_Bus_Init(Birch_IIC_Bus_T * bus)
{
	Birch_IIC_Pin_Init(bus->SCL_GPIOx,bus->SCL_Pin);
	Birch_IIC_Pin_Init(bus->SDA_GPIOx,bus->SDA_Pin);
	
	BIRCH_IIC_SCL_H(bus);
	BIRCH_IIC_SDA_H(bus);
}

/**
 * @brief: 将SDA引脚置为推挽模式，用于主机发送数据的情形
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_SDA_OUT(Birch_IIC_Bus_T *bus)
{
	GPIO_InitTypeDef GPIO_InitStructer;
	
    GPIO_InitStructer.GPIO_Pin	 = bus->SDA_Pin;
    GPIO_InitStructer.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructer.GPIO_Mode	 = GPIO_Mode_Out_PP;
	
    GPIO_Init(bus->SDA_GPIOx, &GPIO_InitStructer);
}

/**
 * @brief: 将SDA引脚置为上拉输入模式，用于接收数据或Ack信号
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_SDA_IN(Birch_IIC_Bus_T *bus)
{
	GPIO_InitTypeDef GPIO_InitStructer;
	
    GPIO_InitStructer.GPIO_Pin	 = bus->SDA_Pin;
    GPIO_InitStructer.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructer.GPIO_Mode	 = GPIO_Mode_IPU;
	
    GPIO_Init(bus->SDA_GPIOx, &GPIO_InitStructer);
}

/**
 * @brief: 发送IIC起始信号
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_Start(Birch_IIC_Bus_T *bus)
{
	Birch_IIC_SDA_OUT(bus);
	
	BIRCH_IIC_SDA_H(bus);
	BIRCH_IIC_SCL_H(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SDA_L(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SCL_L(bus);
}

/**
 * @brief: 发送IIC终止信号
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_Stop(Birch_IIC_Bus_T *bus)
{
	Birch_IIC_SDA_OUT(bus);
	
	BIRCH_IIC_SDA_L(bus);
	BIRCH_IIC_SCL_H(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SDA_H(bus);
		Birch_IIC_Delay(bus);
}

/**
 * @brief: 主机发送后应等待从机的Ack信号，以确认从机是否收到。
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
uint8_t Birch_IIC_WaitAck(Birch_IIC_Bus_T *bus)
{
	uint8_t ucErrTime = 0;
	
	Birch_IIC_SDA_IN(bus);
	
	BIRCH_IIC_SCL_H(bus);
		Birch_IIC_Delay(bus);
	
	while(BIRCH_IIC_READ_SDA(bus))
	{
		ucErrTime++;
		if(ucErrTime > 250)
		{
			return BIRCH_IIC_STATUS_FAILED;
		}
	}
	
	BIRCH_IIC_SCL_L(bus);
		Birch_IIC_Delay(bus);
	
	return BIRCH_IIC_STATUS_OK;
}

/**
 * @brief: 主机发送Ack，示意从机可以继续发送
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_Send_Ack(Birch_IIC_Bus_T *bus)
{
	BIRCH_IIC_SCL_L(bus);
	
	Birch_IIC_SDA_OUT(bus);
	
	BIRCH_IIC_SDA_L(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SCL_H(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SCL_L(bus);
}

/**
 * @brief: 主机不发送Ack，示意从机停止发送
 * @example: None
 * @param:   bus为指定的IIC总线
 * @return:  None
 */
void Birch_IIC_Send_NAck(Birch_IIC_Bus_T *bus)
{
	BIRCH_IIC_SCL_L(bus);
	
	Birch_IIC_SDA_OUT(bus);
	
	BIRCH_IIC_SDA_H(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SCL_H(bus);
		Birch_IIC_Delay(bus);
	
	BIRCH_IIC_SCL_L(bus);
}

/**
 * @brief: 通过IIC总线发送一字节数据
 * @example: None
 * @param:  bus为指定的IIC总线，data为待发送的数据
 * @return:  None
 */
void Birch_IIC_Base_Send_Byte(Birch_IIC_Bus_T *bus, uint8_t data)
{
	Birch_IIC_SDA_OUT(bus);
	
	for(uint8_t i=0;i<8;i++)
	{
		BIRCH_IIC_SCL_L(bus);
			Birch_IIC_Delay(bus);
		
		if(data & 0x80)
			BIRCH_IIC_SDA_H(bus);
		else
			BIRCH_IIC_SDA_L(bus);
		
		data <<= 1;
		
		BIRCH_IIC_SCL_H(bus);
			Birch_IIC_Delay(bus);
		
		BIRCH_IIC_SCL_L(bus);
			Birch_IIC_Delay(bus);
	}
}

/**
 * @brief: 从IIC总线读取一字节数据
 * @example: None
 * @param:  bus为指定的IIC总线，Ack用于响应判断，若为BIRCH_IIC_ACK，则发送响应，示意从机可以继续发送。
 * @return: receive为读取到的数据
 */
uint8_t Birch_IIC_Base_Read_Byte(Birch_IIC_Bus_T *bus,uint8_t Ack)
{
	uint8_t receive = 0;
	Birch_IIC_SDA_IN(bus);
	
	for(uint8_t i=0;i<8;i++)
	{
		BIRCH_IIC_SCL_L(bus);
			Birch_IIC_Delay(bus);
		
		BIRCH_IIC_SCL_H(bus);
		
		receive = (receive << 1) | BIRCH_IIC_READ_SDA(bus);
			Birch_IIC_Delay(bus);
	}
	if(BIRCH_IIC_ACK == Ack)
		Birch_IIC_Send_Ack(bus);
	else
		Birch_IIC_Send_NAck(bus);
	
	return receive;
}
	


/**
 *    _______   __________    __   _      __   __  _             __  __         __  __                  __   __    __
 *   / __/ _ | / __/_  __/   / /  (_)__ _/ /  / /_(_)__  ___ _  / /_/ /  ___   / /_/ /  ___  __ _____ _/ /  / /_  / /
 *  _\ \/ __ |_\ \  / /     / /__/ / _ `/ _ \/ __/ / _ \/ _ `/ / __/ _ \/ -_) / __/ _ \/ _ \/ // / _ `/ _ \/ __/ /_/ 
 * /___/_/ |_/___/ /_/     /____/_/\_, /_//_/\__/_/_//_/\_, /  \__/_//_/\__/  \__/_//_/\___/\_,_/\_, /_//_/\__/ (_)  
 *                                /___/                /___/                                    /___/                
 *                     
 */
