#ifndef __KEY_H
#define __KEY_H

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "bsp_timer.h"

typedef struct {
	GPIO_TypeDef *GPIOx;	//端口
	uint16_t Pin;			//引脚号
	GPIOMode_TypeDef Mode;	//引脚模式
}Birch_Key_Pin_T;



void 	 Key_Mat_Init(void); //矩阵键盘初始化
uint32_t Key_Mat_Scan(void); //矩阵键盘扫描
void 	 Key_Ind_Init(void); //独立按键初始化
uint16_t Key_Ind_Scan(void); //独立按键扫描，最低位为1表示数组中最后一个键按下，依次类推。

#endif 

