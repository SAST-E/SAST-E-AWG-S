#include "gui_port.h"

// a structure which will contain all the data for one display
u8g2_t u8g2;

uint8_t STM32_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
	switch(msg)
	{
		case U8X8_MSG_DELAY_100NANO:		// delay arg_int * 100 nano seconds
			__NOP();
			break;
		case U8X8_MSG_DELAY_10MICRO:		// delay arg_int * 10 micro seconds
			for (uint16_t n = 0; n < 320; n++)
			{
			   __NOP();
			}
			break;
		case U8X8_MSG_DELAY_MILLI:		// delay arg_int * 1 milli second
			Bsp_Delay_ms(1);
			break;											
		case U8X8_MSG_DELAY_I2C:				            
			__NOP();__NOP();__NOP();
			break;							                
		case U8X8_MSG_GPIO_I2C_CLOCK:		                // arg_int=0: Output low at I2C clock pin
			if(arg_int == 1)                                // arg_int=1: Input dir with pullup high for I2C clock pin
				GPIO_WriteBit(GPIOB,GPIO_Pin_8,1);
			else if(arg_int == 0)
				GPIO_WriteBit(GPIOB,GPIO_Pin_8,0);
			break;
		case U8X8_MSG_GPIO_I2C_DATA:			            // arg_int=0: Output low at I2C data pin
			if(arg_int == 1)                                // arg_int=1: Input dir with pullup high for I2C data pin
				GPIO_WriteBit(GPIOB,GPIO_Pin_9,1);
			else if(arg_int == 0)
				GPIO_WriteBit(GPIOB,GPIO_Pin_9,0);
			break;
		case U8X8_MSG_GPIO_MENU_SELECT:
			u8x8_SetGPIOResult(u8x8, /* get menu select pin state */ 0);
			break;
		case U8X8_MSG_GPIO_MENU_NEXT:
			u8x8_SetGPIOResult(u8x8, /* get menu next pin state */ 0);
			break;
		case U8X8_MSG_GPIO_MENU_PREV:
			u8x8_SetGPIOResult(u8x8, /* get menu prev pin state */ 0);
			break;
		case U8X8_MSG_GPIO_MENU_HOME:
			u8x8_SetGPIOResult(u8x8, /* get menu home pin state */ 0);
			break;
		default:
			u8x8_SetGPIOResult(u8x8, 1);			         // default return value
			break;
	}
	return 1;
}


void u8g2_init(void)
{
	// init u8g2 structure
	u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2, U8G2_R0, u8x8_byte_sw_i2c, STM32_gpio_and_delay);  
	// send init sequence to the display, display is in sleep mode after this
	u8g2_InitDisplay(&u8g2);                                                                         
	//wake up display
	u8g2_SetPowerSave(&u8g2, 0); 
	
}

void ShowU8g2Logo(void)
{
	u8g2_ClearBuffer(&u8g2);
	
	u8g2_SetFontMode(&u8g2, 1);
    u8g2_SetFontDirection(&u8g2, 0); /*字体方向选择*/
    u8g2_SetFont(&u8g2, u8g2_font_inb24_mf); /*字库选择*/
    u8g2_DrawStr(&u8g2, 0, 20+20, "U");
    
    u8g2_SetFontDirection(&u8g2, 1);
    u8g2_SetFont(&u8g2, u8g2_font_inb30_mn);
    u8g2_DrawStr(&u8g2, 21,8+20,"8");
        
    u8g2_SetFontDirection(&u8g2, 0);
    u8g2_SetFont(&u8g2, u8g2_font_inb24_mf);
    u8g2_DrawStr(&u8g2, 51,30+20,"g");
    u8g2_DrawStr(&u8g2, 67,30+20,"\xb2");
    
    u8g2_DrawHLine(&u8g2, 2, 35+20, 47);
    u8g2_DrawHLine(&u8g2, 3, 36+20, 47);
    u8g2_DrawVLine(&u8g2, 45, 32+20, 12);
    u8g2_DrawVLine(&u8g2, 46, 33+20, 12);
	
	u8g2_SendBuffer(&u8g2);
}