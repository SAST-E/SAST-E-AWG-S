
#include "wave_generator.h"


const uint16_t DAC_Wave_Length = 50;

uint16_t DAC_WaveTable_Counter = 0;

uint16_t DAC_WaveTable[DAC_Wave_Length] = {0};
//正弦的话，计算出来的值需要做一点调整，所以不实时计算。
uint16_t DAC_WaveTable_Sin_8bit[DAC_Wave_Length] = { \
	128, 144, 160, 175, 190, 203, 216, 227, 236, 244, \
	250, 254, 255, 255, 254, 250, 244, 236, 227, 216, \
	203, 190, 175, 160, 144, 128, 112, 96,  81,  66,  \
	53,  40,  29,  20,  12,  6,   2,   1,   1,   2,   \
	6,   12,  20,  29,  40,  53,  66,  81,  96,  112};

uint16_t DAC_WaveTable_Sin_10bit[DAC_Wave_Length] = { \
	512, 576, 639, 700, 759, 813, 862, 907, 944, 975, \
	999, 1015,1023,1023,1015,999, 975, 944, 907, 862, \
	813, 759, 700, 639, 576, 512, 448, 385, 324, 265, \
	211, 162, 117, 80,  49,  25,  9,   1,   1,   9,   \
	25,  49,  80,  117, 162, 211, 265, 324, 385, 448};

uint16_t DAC_WaveTable_Sin_12bit[DAC_Wave_Length] = { \
	2048, 2305, 2557, 2802, 3035, 3252, 3450, 3626, 3777, 3901, \
	3996, 4060, 4092, 4092, 4060, 3996, 3901, 3777, 3626, 3450, \
	3252, 3035, 2802, 2557, 2305, 2048, 1791, 1539, 1294, 1061, \
	844,  646,  470,  319,  195,  100,  36,   4,    4,    36, 	\
	100,  195,  319,  470,  646,  844,  1061, 1294, 1539, 1791};

uint16_t DAC_WaveTable_Sin_14bit[DAC_Wave_Length] = { \
	8192,  9219,  10229, 11208, 12139, 13007, 13800, 14504, 15109, 15604, \
	15983, 16239, 16368, 16368, 16239, 15983, 15604, 15109, 14504, 13800, \
	13007, 12139, 11208, 10229, 9219,  8192,  7165,  6155,  5176,  4245,  \
	3377,  2584,  1880,  1275,  780,   401,   145,   16,    16,    145,   \
	401,   780,   1275,  1880,  2584,  3377,  4245,  5176,  6155,  7165};

//外部高速并行DAC负责波形数据的输出
void ParallelDAC_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);

	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);  
	
	GPIO_InitTypeDef GPIOInitStructure;
	GPIOInitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIOInitStructure.GPIO_Pin   = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7|  \
								   GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12|GPIO_Pin_13;
	GPIOInitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOB,&GPIOInitStructure);
}

inline void ParallelDAC_Write(uint16_t value)
{
	GPIOB->BSRR = (uint16_t)value;
	GPIOB->BRR  = (uint16_t)(~value);
}

//外部低速串行DAC负责偏移电压的产生
void SerialDAC_Init(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	
	GPIO_InitTypeDef GPIOInitStructure;
	
	GPIOInitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIOInitStructure.GPIO_Pin = GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_7;
	GPIOInitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA,&GPIOInitStructure);
	
	GPIO_SetBits(GPIOA,GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_7);
}

void SerialDAC_Write(uint16_t data)
{
	//ad5621在上升沿锁存数据
#if defined AD5621
	
	data <<= 4;//抛弃高四位
	data >>= 2;//D0在Bit2位置
	
#endif
	
	GPIO_ResetBits(GPIOA, SerialDAC_Pin_CLK);//时钟拉低
	__nop();__nop();__nop();
	GPIO_ResetBits(GPIOA, SerialDAC_Pin_CS);//片选拉低
	
	__nop();__nop();__nop();
	
	for(uint8_t i = 0;i<16;i++)
	{
		GPIO_SetBits(GPIOA, SerialDAC_Pin_CLK);//时钟拉高
		if(data & 0x8000)
		{
			GPIO_WriteBit(GPIOA,SerialDAC_Pin_MOSI,1);
		}
		else
		{
			GPIO_WriteBit(GPIOA,SerialDAC_Pin_MOSI,0);
		}
		data <<= 1;
		__nop();__nop();__nop();
		
		GPIO_ResetBits(GPIOA, SerialDAC_Pin_CLK);//时钟拉低,锁存数据
		__nop();__nop();__nop();
	}
	
	GPIO_SetBits(GPIOA, SerialDAC_Pin_CS);//片选拉高
	__nop();__nop();__nop();
	GPIO_SetBits(GPIOA, SerialDAC_Pin_CLK);//时钟拉高
	
}

//TIM2产生一路PWM
void Timer2_Init(uint16_t arr,uint16_t psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);

    TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
    TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值
    TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位


	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);


	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = arr/2;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC4Init(TIM2,&TIM_OCInitStructure);
	
	
	TIM_CtrlPWMOutputs(TIM2,ENABLE);
	TIM_OC1PreloadConfig(TIM2,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM2,ENABLE);
	
    TIM_Cmd(TIM2, ENABLE);  //使能TIMx外设
	
}

//TIM3负责更新中断与时钟信号的产生
void Timer3_Init(uint16_t arr,uint16_t psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);

    TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值
    TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值
    TIM_TimeBaseStructure.TIM_ClockDivision = 0; //设置时钟分割:TDTS = Tck_tim
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位


    TIM_ITConfig(  //使能或者失能指定的TIM中断
        TIM3, //TIM3
        TIM_IT_Update  |  //TIM 中断源
        TIM_IT_Trigger,   //TIM 触发中断源
        ENABLE  //使能
        );

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);


	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse = arr/2;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init(TIM3,&TIM_OCInitStructure);
	
	
	TIM_CtrlPWMOutputs(TIM3,ENABLE);
	TIM_OC1PreloadConfig(TIM3,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM3,ENABLE);
	

    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;  //TIM3中断
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级0级
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;  //从优先级3级
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
    NVIC_Init(&NVIC_InitStructure);  //根据NVIC_InitStruct中指定的参数初始化外设NVIC寄存器

    TIM_Cmd(TIM3, ENABLE);  //使能TIMx外设
	
}

void TIM3_IRQHandler(void)   //TIM3中断，更新并行DAC数据
{
    //if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源
		//TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );  //清除TIMx的中断待处理位:TIM 中断源
		TIM3->SR = (uint16_t)~TIM_IT_Update;
		
		ParallelDAC_Write(DAC_WaveTable[DAC_WaveTable_Counter]);//更新通道一数据
		DAC_WaveTable_Counter++;
		if(DAC_WaveTable_Counter == DAC_Wave_Length)
		{
			DAC_WaveTable_Counter = 0;
		}
}



//根据传入数据计算波表
void WaveTableGenerate(int Wave, int Amp, int Duty)
{
	//校正系数，补偿幅度的损失。
	//暂时只是在1K下校准一下，其实应该做个线性拟合（线性？大概？）
	float Correct_Sin = 0.95;
	float Correct_Triangle = 0.95;
	float Correct_Square = 0.95;
	const int AmpMax = 10200;//减法器做了5.1倍的放大（和具体焊接的电阻有关）
	
	switch(Wave)
	{
		case 0://sin
		{
			for(uint8_t i=0;i<DAC_Wave_Length;i++) {
				//DAC_WaveTable[i] = 127 + (int8_t)(Amp/(AmpMax * Correct_Sin) * 127 * sin((i+1)/(DAC_Wave_Length*1.0) * 2 * PI));
				
				//使用预先计算好的波表
			#ifdef AD9708
				DAC_WaveTable[i] = 128  + (DAC_WaveTable_Sin_8bit[i]  - 128)*Amp/(AmpMax * Correct_Sin);
				if(DAC_WaveTable[i] > DAC_MaxValue) {
					DAC_WaveTable[i] = DAC_MaxValue;
				}
			#endif
				
			#ifdef AD9760
				DAC_WaveTable[i] = 512  + (DAC_WaveTable_Sin_10bit[i] - 512)*Amp/(AmpMax * Correct_Sin);
				if(DAC_WaveTable[i] > DAC_MaxValue) {
					DAC_WaveTable[i] = DAC_MaxValue;
				}
			#endif
				
			#ifdef AD9762
				DAC_WaveTable[i] = 2048 + (DAC_WaveTable_Sin_12bit[i] - 2048)*Amp/(AmpMax * Correct_Sin);
				if(DAC_WaveTable[i] > DAC_MaxValue) {
					DAC_WaveTable[i] = DAC_MaxValue;
				}
			#endif

			#ifdef AD9764
				DAC_WaveTable[i] = 8192 + (DAC_WaveTable_Sin_14bit[i] - 8192)*Amp/(AmpMax * Correct_Sin);
				if(DAC_WaveTable[i] > DAC_MaxValue) {
					DAC_WaveTable[i] = DAC_MaxValue;
				}
			#endif		
				
				DAC_WaveTable[i] <<= DAC_BitOffset;
			}
		}
			break;
		
		case 1://triangle
		{
			for(uint8_t i=0;i<DAC_Wave_Length;i++) 
			{
				if(i < DAC_Wave_Length/2) {
					DAC_WaveTable[i] = DAC_ZeroPointValue + (Amp)/(AmpMax * Correct_Triangle) * DAC_MaxValue * (i/(DAC_Wave_Length/2.0) - 0.5);
				}
				else {
					DAC_WaveTable[i] = DAC_ZeroPointValue + (Amp)/(AmpMax * Correct_Triangle) * DAC_MaxValue * ((DAC_Wave_Length - i)/(DAC_Wave_Length/2.0) - 0.5);
				}
				
				DAC_WaveTable[i] <<= DAC_BitOffset;
			}
		}
			break;
		
		case 2://square
		{
			for(uint8_t i=0;i<DAC_Wave_Length;i++) 
			{
				if(i < DAC_Wave_Length/2) {
					DAC_WaveTable[i] = DAC_ZeroPointValue + ((Amp)/(AmpMax * Correct_Square) * DAC_HalfValue);
				}
				else {
					DAC_WaveTable[i] = DAC_ZeroPointValue + ((Amp)/(AmpMax * Correct_Square) * -DAC_HalfValue);
				}
				
				DAC_WaveTable[i] <<= DAC_BitOffset;
			}
		}
			break;
		
		case 3://pulse
		{
			for(uint8_t i=0;i<DAC_Wave_Length;i++) 
			{
				if(i/(DAC_Wave_Length*1.0) * 100 < Duty) {
					//DAC_WaveTable[i] = 127 + (int8_t)((Amp/2)/(AmpMax * Correct_Square) * 127);
					DAC_WaveTable[i] = DAC_ZeroPointValue + ((Amp)/(AmpMax * Correct_Square) * -DAC_HalfValue);
				}
				else {
					//DAC_WaveTable[i] = 127 + (int8_t)((Amp/2)/(AmpMax * Correct_Square) * -127);
					DAC_WaveTable[i] = DAC_ZeroPointValue + ((Amp)/(AmpMax * Correct_Square) * DAC_HalfValue);
				}
				
				DAC_WaveTable[i] <<= DAC_BitOffset;
			}
		}
			break;
		
		default:
		{	
		}
			
	}
}

void Offset_Set(int Offset)
{
	#ifdef AD9708
		#ifdef AD5621
	
			uint16_t offset_value = (Offset/5000.0 * 2048) + 2048 - 8;//最后一个数据是对并行DAC不能到零的一个补偿
			if(offset_value > 4095)
			{
				offset_value = 4095;
			}
			SerialDAC_Write(offset_value);
			
		#endif
	#endif
}

//调用函数计算波表并调整中断频率
void AwgSet(int Ch, int Fre, int Amp, int Phase, int Duty, int Offset, int Wave)
{
	uint16_t psc = 0;
	uint16_t arr = 0;
	
	if(Ch == 1)
	{
		TIM_Cmd(TIM3,DISABLE);
		TIM_ARRPreloadConfig(TIM3,DISABLE);
		
		WaveTableGenerate(Wave, Amp, Duty);
		
		//阈值限制
		if(Fre > 10000)
		{
			Fre = 10000;
		}
		else if(Fre < 1)
		{
			Fre = 1;
		}
		
		//其实可以写一个算法来计算psc、arr取何值能获取最佳精度，但是那个计算量对于F1来说可能太大了，所以就简单分段处理一下。
		//这里在不同的段内psc取一个固定值，故分段的原则就是保证arr的数值尽量大（以获取更高精度）
		if(Fre < 20) {
			psc = 8;	//9
		}
		else {
			psc = 1;	//2
		}
		
		arr = (uint16_t)(72000000/(Fre * DAC_Wave_Length * (psc+1) * 1.0) - 1);
	
		TIM3->ARR  = arr;
		TIM3->PSC  = psc;
		TIM3->CCR1 = arr/2;
		
		TIM_ARRPreloadConfig(TIM3,ENABLE);
		TIM_Cmd(TIM3,ENABLE);
		
		Offset_Set(Offset);
		
	}
	else if(Ch == 2)
	{
		TIM_Cmd(TIM2,DISABLE);
		TIM_ARRPreloadConfig(TIM2,DISABLE);
		
		//阈值限制
		if(Fre > 360000)
		{
			Fre = 360000;
		}
		else if(Fre < 1)
		{
			Fre = 1;
		}
		
		if(Fre < 10) {
			psc = 7199;	//7200分频
		}
		else if(Fre < 100) {
			psc = 719;	//720分频
		}
		else if(Fre < 7200) {
			psc = 71;	//72分频
		}
		else {
			psc = 0;	//不分频
		}
		
		arr = (uint16_t)(72000000/(Fre * (psc+1) * 1.0) - 1);
	
		TIM2->ARR  = arr;
		TIM2->PSC  = psc;
		TIM2->CCR4 = (uint16_t)((arr * Duty)/100.0);
		
		//特殊处理一下
		if(Duty == 100)
		{
			GPIO_SetBits(GPIOA,GPIO_Pin_4);
		}
		else if(Duty == 0)
		{
			GPIO_ResetBits(GPIOA,GPIO_Pin_4);
		}
		else
		{
			TIM_ARRPreloadConfig(TIM2,ENABLE);
			TIM_Cmd(TIM2,ENABLE);
		}
	}
	
}

//Json字符串处理函数
void Json_Process(void)
{
	uint16_t len  = 0;
	
	int amplitude = 0;	//幅度
	int channel   = 0;	//通道
	int duty 	  = 0;  //占空比
	int frequency = 0;	//频率
	int offset 	  = 0;  //偏移电压
	int phase 	  = 0;	//相位
	const char * wave;  //波形种类
	char json_RX_buffer[400] =  {'\0'}; //json字符串暂存区域
	char CommandBuffer[50] = {""};  //指令Buffer
	
	json_t *awg_raw;
	json_error_t error;

	if(usart1_rx_ok == 1)
	{
		usart1_rx_ok = 0;
		len = usart1_rx_count;
		usart1_rx_count = 0;
		
		for(uint16_t i=0;i<len;i++) {
			json_RX_buffer[i] = usart1_rx_buffer[i];
		}
		
		//非json数据
		if(json_RX_buffer[0] != '{')
		{
			//判断是否为指令请求
			if(json_RX_buffer[0] >= 'a' && json_RX_buffer[0] <= 'z' && len < 50)//指令请求，仅支持小写字母，指令最大长度为49字节
			{
				//复制接收到的内容到指令缓冲并在末尾补‘\0’使内容能够被识别为字符串
				strncpy(CommandBuffer, json_RX_buffer, len);
				CommandBuffer[len] = '\0';
				
				switch(len)
				{
					case 3: //ver
					{
						if(strcmp(CommandBuffer, "ver") == 0) //版本信息请求
						{
							//打印自身版本信息
							printf("SAST-E-AWG-S1-V0.1\n");
							printf("%d",len);
						}
						else
						{
							goto CommandError;
						}
					}break;
					
					case 4: //help
					{
						if(strcmp(CommandBuffer, "help") == 0) //帮助信息请求
						{
							printf("Command list:\n");
							printf("ver/version ----show version information.\n");
							printf("detail 	----show the details about settings.\n");
							
						}
						else
						{
							goto CommandError;
						}
					}break;
					
					case 6: //detail
					{
						if(strcmp(CommandBuffer, "detail") == 0) //请求显示详细配置
						{
							printf("Hardware Information:\n");
							printf("Parallel DAC: %d Bit, Serial DAC: %d Bit\n", 14 - DAC_BitOffset, 12);
							printf("Firmware version: V0.1\n");//发布之前保持V0.1吧，懒得写更新记录

						}
						else
						{
							goto CommandError;
						}
					}break;
					
					case 7: //version
					{
						if(strcmp(CommandBuffer, "version") == 0) //还是版本信息请求
						{
							//打印自身版本信息
							printf("SAST-E-AWG-S1-V0.1\n");
						}
						else
						{
							goto CommandError;
						}
					}break;
					
					default://CommandError
					{
						goto CommandError;
					}
				}
					
			}
			else
			{	
				CommandError:
				{
					printf("Command error! Please check input.\n");
				}
			}
		}
		else//非指令请求，则将输入作为指定格式的json字符串来解析。
		{
			awg_raw = json_loads(json_RX_buffer, JSON_ENCODE_ANY, &error);
			
			//测试用例：{"amplitude":2000,"channel":1,"duty":90,"frequency":500,“offset”:0,"phase":0,"wave":"sin"}
			
			json_unpack(awg_raw,"{s:i,s:i,s:i,s:i,s:i,s:i,s:s}", 	\
									"amplitude",&amplitude, 	\
									"channel",  &channel, 		\
									"duty", 	&duty, 			\
									"frequency",&frequency, 	\
									"offset", 	&offset, 		\
									"phase", 	&phase, 		\
									"wave", 	&wave );
			
			//通过串口打印接收到的数据以验证
//			printf("Amp:\t Ch:\t Fre:\t Pha:\t Wave:\n");
//			printf("%d \t %d \t %d \t %d \t",amplitude, channel, frequency, phase);
//			printf("%s\n", wave);
			
			if(		0 == strcmp(wave, "sin")) {
				AwgSet(channel, frequency, amplitude, phase, duty, offset, 0 );
			}
			else if(0 == strcmp(wave, "triangle")) {
				AwgSet(channel, frequency, amplitude, phase, duty, offset, 1 );
			}
			else if(0 == strcmp(wave, "square")) {
				AwgSet(channel, frequency, amplitude, phase, duty, offset, 2 );
			}
			else if(0 == strcmp(wave, "pulse")) {
				AwgSet(channel, frequency, amplitude, phase, duty, offset, 3 );
			}
			
			
			
			json_delete(awg_raw);	//删除json对象
			memset(json_RX_buffer,0x00,sizeof(json_RX_buffer)); //清空数组
		}
	}
}


void GPIO_TogglePin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	/* Check the parameters */
	assert_param(IS_GPIO_ALL_PERIPH(GPIOx));
	assert_param(IS_GPIO_PIN(GPIO_Pin));

	if ((GPIOx->ODR & GPIO_Pin) != 0x00)
	{
		GPIOx->BRR = (uint32_t)GPIO_Pin;
	}
	else
	{
		GPIOx->BSRR = (uint32_t)GPIO_Pin;
	}
}

/****************下面是用于计算波表的Python程序********************/
//就是野火的程序啦
//算了51点，抛掉最后一点，计算8bit波表时出来的数据不对称，需要手动调整一下。

//import matplotlib.pyplot as plt
//import numpy as np
//import math

//#修改本变量可以更改点数，如16、32、64等
//POINT_NUM = 51

//pi = math.pi

//#一个周期 POINT_NUM 个点
//n = np.linspace(0,2*pi,POINT_NUM)

//#计算POINT_NUM个点的正弦值
//a = map(math.sin,n)

//r =[]
//for i in a:
//    #调整幅值至在0~1区间
//    i+=1

//    #按3.3V电压调整幅值
//    i*= 10.2/2

//    #求取dac数值，12位dac LSB = 3.3V/2**12
//    ri = round(i*2**10/10.2)

//    #检查参数
//    if ri >= 1023:
//        ri = 1023

//    #得到dac数值序列
//    r.append( ri )

//print(list(map(int,r)))

//#写入序列到文件
//with open("py_dac_sinWav.c",'w',encoding= 'gb2312') as f:
//    print(list(map(int,r)),file= f)

//#绘图
//plt.plot(n,r,"-o")
//plt.show()

