#include "bsp.h"
#include "birch_iic.h"
#include "u8g2.h"
#include "gui_port.h"
#include "math.h"
#include <string.h>
#include "wave_generator.h"

extern u8g2_t u8g2; 


int main()
{
	//生成波形数据查找表
	WaveTableGenerate(0, 3000, 25);//3Vpp的正弦（可惜这里不是cpp搞不了重载）
	
	Bsp_Init();//BSP初始化
	
	//12864 OLED 初始化
//	Birch_IIC_Bus_Init(&birch_iic_bus[1]);
//	u8g2_init();
//	ShowU8g2Logo();
	
	ParallelDAC_Init();
	
	Timer2_Init(999,71);//默认输出1K方波(IO直出)
	Timer3_Init(19, 71);//默认50K中断速率，DAC输出1K正弦

	SerialDAC_Init();
	SerialDAC_Write(2048);
	
	Board_LED_Blink(2,200);//闪灯提示
	
	while(1)
	{
		for(int i=0;i<10000;i++) {
			Json_Process();//处理json并调整工作状态等
		}
		
		//不能再开中断占资源了所以只能轮询一下才能实现这样子
		GPIO_TogglePin(BOARD_LED_PORT,BOARD_LED_PIN); //翻转LED以指示设备正在工作
	}
}















