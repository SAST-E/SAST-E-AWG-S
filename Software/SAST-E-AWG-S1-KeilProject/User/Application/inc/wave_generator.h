#ifndef __WAVE_GENERATOR_H
#define __WAVE_GENERATOR_H

#include "bsp.h"
#include <jansson.h> //Json字符串处理所需的库，如果没有请在Keil官网下载相应Pack来安装

#define PI 3.1415926

#define SerialDAC_Pin_CS 	GPIO_Pin_4
#define SerialDAC_Pin_CLK 	GPIO_Pin_5
#define SerialDAC_Pin_MOSI 	GPIO_Pin_7

//#define AD5601
#define AD5621 

#define AD9708
//#define AD9760
//#define AD9762
//#define AD9764

#ifdef AD9708

	#define DAC_ZeroPointValue  127 //所用并行DAC为差分电流型，两路电流无法相等，故不能输出零，这里只是一个近似
	#define DAC_MaxValue 		255
	#define DAC_HalfValue 		127
	#define DAC_BitOffset		6	//AD9708为8bitDAC，该系列使用14bit数据接口，最高位的位置始终不变，故需要左移6位
	
#endif

#ifdef AD9760

	#define DAC_ZeroPointValue  511 //所用并行DAC为差分电流型，两路电流无法相等，故不能输出零，这里只是一个近似
	#define DAC_MaxValue 		1023
	#define DAC_BitOffset		4	//AD9760为10bitDAC，该系列使用14bit数据接口，最高位的位置始终不变，故需要左移4位
	
#endif

#ifdef AD9762

	#define DAC_ZeroPointValue  2047 //所用并行DAC为差分电流型，两路电流无法相等，故不能输出零，这里只是一个近似
	#define DAC_MaxValue 		4095
	#define DAC_BitOffset		2	//AD9762为12bitDAC，该系列使用14bit数据接口，最高位的位置始终不变，故需要左移2位
	
#endif

#ifdef AD9764

	#define DAC_ZeroPointValue  8191 //所用并行DAC为差分电流型，两路电流无法相等，故不能输出零，这里只是一个近似
	#define DAC_MaxValue 		16383
	#define DAC_BitOffset		0	//AD9764为14bitDAC，该系列使用14bit数据接口，最高位的位置始终不变，不需要左移
	
#endif

//DAC相关函数
void ParallelDAC_Init(void);
inline void ParallelDAC_Write(uint16_t value);
void SerialDAC_Init(void);
void SerialDAC_Write(uint16_t data);

//定时器初始化
void Timer2_Init(uint16_t arr,uint16_t psc);
void Timer3_Init(uint16_t arr,uint16_t psc);

//波表及定时器参数的设置
void WaveTableGenerate(int Wave, int Amp, int Duty);
void AwgSet(int Ch, int Fre, int Amp, int Phase, int Duty, int Offset, int Wave);

//处理Json
void Json_Process(void);


#endif

