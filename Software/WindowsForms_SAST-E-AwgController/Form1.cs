﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Drawing.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace WindowsForms_AwgController
{
    public partial class Form1 : Form
    {
        //定义串口类
        private SerialPort ComDevice = new SerialPort();
        private char[] DeviceInfo = new char[200];
        public Form1()
        {
            InitializeComponent();
            
            InitialConfig();
        }

        public class AWG
        {
            public int amplitude { get; set; }
            public int channel { get; set; }
            public int duty { get; set; }
            public int frequency { get; set; }
            public int offset { get; set; }
            public int phase { get; set; }
            public string wave { get; set; }
        }

        //串口初始化
        private void InitialConfig()
        {
            string Version = "V0.1";

            //设置窗口标题
            this.Text = "电子部开源仪器控制台——信号发生器" + Version;

            comboBox_Port.Items.AddRange(SerialPort.GetPortNames());
           
            if(comboBox_Port.Items.Count > 0)
            {
                comboBox_Port.SelectedIndex = 0;
            }
            else
            {
                comboBox_Port.Text = "未检测到串口";
            }

            System.Object[] channel = { 1, 2 };
            System.Object[] wave = { "sin", "triangle", "square", "pulse" };
            System.Object[] baud = { 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 57600, 115200, 128000, 256000 };

            comboBox_Channel.Items.AddRange(channel);
            comboBox_Channel.SelectedIndex = 0;

            comboBox_Wave.Items.AddRange(wave);
            comboBox_Wave.SelectedIndex = 0;

            comboBox_Baud.Items.AddRange(baud);
            comboBox_Baud.SelectedIndex = 10;//默认波特率115200

            numericUpDown_Amplitude.Value = 3000;
            numericUpDown_Frequency.Value = 1000;
            numericUpDown_Phase.Value = 0;
            numericUpDown_Duty.Value = 50;

            button_Send.Enabled = false;

        }

        private void buttonSend_Click(object sender, EventArgs e)
        {

            AWG Awg = new AWG();
            
            Awg.channel = Convert.ToInt32(comboBox_Channel.SelectedItem.ToString());
            Awg.amplitude = (int)numericUpDown_Amplitude.Value;
            Awg.frequency = (int)numericUpDown_Frequency.Value;
            Awg.offset = (int)numericUpDown_Offset.Value;
            Awg.phase = (int)numericUpDown_Phase.Value;
            Awg.duty = (int)numericUpDown_Duty.Value;
            Awg.wave = comboBox_Wave.Text;

            string output = JsonConvert.SerializeObject(Awg);

            string data_send = output + "\r\n";

            ComDevice.Write(data_send);
            
            //MessageBox.Show(output, "输出信息", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button_ComSwitch_Click(object sender, EventArgs e)
        {
            if(comboBox_Port.Items.Count <= 0)
            {
                MessageBox.Show("未发现可用串口，请检查硬件设备");
                return;
            }

            if(ComDevice.IsOpen == false)
            {
                ComDevice.PortName = comboBox_Port.SelectedItem.ToString();
                ComDevice.BaudRate = Convert.ToInt32(comboBox_Baud.SelectedItem.ToString());
                ComDevice.DataBits = 8;
                ComDevice.StopBits = StopBits.One;
                ComDevice.Parity = Parity.None;
                
                try
                {
                    ComDevice.Open();

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "未能成功开启串口", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                button_ComSwitch.Text = "关闭串口";
                button_Send.Enabled = true;

            }
            else
            {
                try
                {
                    //关闭串口
                    ComDevice.Close();
                    button_Send.Enabled = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "串口关闭错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                button_ComSwitch.Text = "打开串口";

            }
        }

        private void button_ReDefaultSettings_Click(object sender, EventArgs e)
        {

            comboBox_Channel.SelectedIndex = 0;
            comboBox_Wave.SelectedIndex = 0;
            comboBox_Baud.SelectedIndex = 10;//默认波特率115200

            numericUpDown_Amplitude.Value = 3000;
            numericUpDown_Frequency.Value = 1000;
            numericUpDown_Phase.Value = 0;
            numericUpDown_Duty.Value = 50;
        }

        private void comboBox_Channel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox_Channel.SelectedIndex == 0)//sin、triangle、square、pulse
            {
                numericUpDown_Frequency.Maximum = 10000;
                numericUpDown_Amplitude.Enabled = true;
                numericUpDown_Offset.Enabled = true;
                numericUpDown_Phase.Enabled = true;
                if (comboBox_Wave.SelectedIndex == 3)
                {
                    numericUpDown_Duty.Enabled = true;
                }
                else
                {
                    numericUpDown_Duty.Enabled = false;
                }
                
                comboBox_Wave.Enabled = true;
            }
            else if(comboBox_Channel.SelectedIndex == 1)// pwm out
            {
                numericUpDown_Frequency.Maximum = 360000;
                numericUpDown_Amplitude.Enabled = false;
                numericUpDown_Duty.Enabled = true;
                numericUpDown_Offset.Enabled = false;
                numericUpDown_Phase.Enabled = false;
                comboBox_Wave.Enabled = false;
            }
        }

        private void comboBox_Wave_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox_Wave.SelectedIndex == 3)//pulse
            {
                numericUpDown_Duty.Enabled = true;
            }
            else
            {
                numericUpDown_Duty.Enabled = false;
            }
        }
    }
}
