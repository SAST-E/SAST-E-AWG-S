﻿namespace WindowsForms_AwgController
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Send = new System.Windows.Forms.Button();
            this.comboBox_Port = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_Baud = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDown_Frequency = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Amplitude = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Phase = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Duty = new System.Windows.Forms.NumericUpDown();
            this.comboBox_Channel = new System.Windows.Forms.ComboBox();
            this.comboBox_Wave = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button_ComSwitch = new System.Windows.Forms.Button();
            this.button_ReDefaultSettings = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown_Offset = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Frequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Amplitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Phase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Duty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Offset)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Send
            // 
            this.button_Send.Font = new System.Drawing.Font("等线", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_Send.Location = new System.Drawing.Point(370, 248);
            this.button_Send.Name = "button_Send";
            this.button_Send.Size = new System.Drawing.Size(208, 51);
            this.button_Send.TabIndex = 0;
            this.button_Send.Text = "设置";
            this.button_Send.UseVisualStyleBackColor = true;
            this.button_Send.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // comboBox_Port
            // 
            this.comboBox_Port.FormattingEnabled = true;
            this.comboBox_Port.Location = new System.Drawing.Point(135, 57);
            this.comboBox_Port.Name = "comboBox_Port";
            this.comboBox_Port.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Port.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(44, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "串口号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(44, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "波特率：";
            // 
            // comboBox_Baud
            // 
            this.comboBox_Baud.FormattingEnabled = true;
            this.comboBox_Baud.Location = new System.Drawing.Point(135, 87);
            this.comboBox_Baud.Name = "comboBox_Baud";
            this.comboBox_Baud.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Baud.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(366, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "通道：";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Label4.Location = new System.Drawing.Point(366, 58);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(66, 19);
            this.Label4.TabIndex = 7;
            this.Label4.Text = "频率：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(366, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "幅度：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(366, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 19);
            this.label6.TabIndex = 11;
            this.label6.Text = "相位：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(366, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 19);
            this.label7.TabIndex = 13;
            this.label7.Text = "占空比：";
            // 
            // numericUpDown_Frequency
            // 
            this.numericUpDown_Frequency.Location = new System.Drawing.Point(438, 58);
            this.numericUpDown_Frequency.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_Frequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_Frequency.Name = "numericUpDown_Frequency";
            this.numericUpDown_Frequency.Size = new System.Drawing.Size(121, 21);
            this.numericUpDown_Frequency.TabIndex = 16;
            this.numericUpDown_Frequency.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_Amplitude
            // 
            this.numericUpDown_Amplitude.Location = new System.Drawing.Point(438, 85);
            this.numericUpDown_Amplitude.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_Amplitude.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Amplitude.Name = "numericUpDown_Amplitude";
            this.numericUpDown_Amplitude.Size = new System.Drawing.Size(121, 21);
            this.numericUpDown_Amplitude.TabIndex = 17;
            this.numericUpDown_Amplitude.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // numericUpDown_Phase
            // 
            this.numericUpDown_Phase.Location = new System.Drawing.Point(438, 139);
            this.numericUpDown_Phase.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericUpDown_Phase.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.numericUpDown_Phase.Name = "numericUpDown_Phase";
            this.numericUpDown_Phase.Size = new System.Drawing.Size(121, 21);
            this.numericUpDown_Phase.TabIndex = 18;
            // 
            // numericUpDown_Duty
            // 
            this.numericUpDown_Duty.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown_Duty.Location = new System.Drawing.Point(438, 166);
            this.numericUpDown_Duty.Name = "numericUpDown_Duty";
            this.numericUpDown_Duty.Size = new System.Drawing.Size(121, 21);
            this.numericUpDown_Duty.TabIndex = 19;
            // 
            // comboBox_Channel
            // 
            this.comboBox_Channel.FormattingEnabled = true;
            this.comboBox_Channel.Location = new System.Drawing.Point(438, 30);
            this.comboBox_Channel.Name = "comboBox_Channel";
            this.comboBox_Channel.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Channel.TabIndex = 20;
            this.comboBox_Channel.SelectedIndexChanged += new System.EventHandler(this.comboBox_Channel_SelectedIndexChanged);
            // 
            // comboBox_Wave
            // 
            this.comboBox_Wave.FormattingEnabled = true;
            this.comboBox_Wave.Location = new System.Drawing.Point(438, 193);
            this.comboBox_Wave.Name = "comboBox_Wave";
            this.comboBox_Wave.Size = new System.Drawing.Size(121, 20);
            this.comboBox_Wave.TabIndex = 22;
            this.comboBox_Wave.SelectedIndexChanged += new System.EventHandler(this.comboBox_Wave_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(366, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 19);
            this.label8.TabIndex = 21;
            this.label8.Text = "波形：";
            // 
            // button_ComSwitch
            // 
            this.button_ComSwitch.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_ComSwitch.Location = new System.Drawing.Point(48, 191);
            this.button_ComSwitch.Name = "button_ComSwitch";
            this.button_ComSwitch.Size = new System.Drawing.Size(208, 51);
            this.button_ComSwitch.TabIndex = 23;
            this.button_ComSwitch.Text = "打开串口";
            this.button_ComSwitch.UseVisualStyleBackColor = true;
            this.button_ComSwitch.Click += new System.EventHandler(this.button_ComSwitch_Click);
            // 
            // button_ReDefaultSettings
            // 
            this.button_ReDefaultSettings.Font = new System.Drawing.Font("等线", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_ReDefaultSettings.Location = new System.Drawing.Point(48, 248);
            this.button_ReDefaultSettings.Name = "button_ReDefaultSettings";
            this.button_ReDefaultSettings.Size = new System.Drawing.Size(208, 51);
            this.button_ReDefaultSettings.TabIndex = 24;
            this.button_ReDefaultSettings.Text = "恢复默认";
            this.button_ReDefaultSettings.UseVisualStyleBackColor = true;
            this.button_ReDefaultSettings.Click += new System.EventHandler(this.button_ReDefaultSettings_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(565, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 19);
            this.label10.TabIndex = 29;
            this.label10.Text = "%";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(565, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 19);
            this.label11.TabIndex = 28;
            this.label11.Text = "°";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(565, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 19);
            this.label12.TabIndex = 27;
            this.label12.Text = "mVpp";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(565, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 19);
            this.label13.TabIndex = 26;
            this.label13.Text = "Hz";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(565, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 19);
            this.label9.TabIndex = 32;
            this.label9.Text = "mVpp";
            // 
            // numericUpDown_Offset
            // 
            this.numericUpDown_Offset.Location = new System.Drawing.Point(438, 112);
            this.numericUpDown_Offset.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown_Offset.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            -2147483648});
            this.numericUpDown_Offset.Name = "numericUpDown_Offset";
            this.numericUpDown_Offset.Size = new System.Drawing.Size(121, 21);
            this.numericUpDown_Offset.TabIndex = 31;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("等线", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(366, 112);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 19);
            this.label14.TabIndex = 30;
            this.label14.Text = "偏移：";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 328);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numericUpDown_Offset);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button_ReDefaultSettings);
            this.Controls.Add(this.button_ComSwitch);
            this.Controls.Add(this.comboBox_Wave);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox_Channel);
            this.Controls.Add(this.numericUpDown_Duty);
            this.Controls.Add(this.numericUpDown_Phase);
            this.Controls.Add(this.numericUpDown_Amplitude);
            this.Controls.Add(this.numericUpDown_Frequency);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox_Baud);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_Port);
            this.Controls.Add(this.button_Send);
            this.Name = "Form1";
            this.Text = "电子部开源仪器控制台——信号发生器";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Frequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Amplitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Phase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Duty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Offset)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Send;
        private System.Windows.Forms.ComboBox comboBox_Port;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_Baud;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_Frequency;
        private System.Windows.Forms.NumericUpDown numericUpDown_Amplitude;
        private System.Windows.Forms.NumericUpDown numericUpDown_Phase;
        private System.Windows.Forms.NumericUpDown numericUpDown_Duty;
        private System.Windows.Forms.ComboBox comboBox_Channel;
        private System.Windows.Forms.ComboBox comboBox_Wave;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_ComSwitch;
        private System.Windows.Forms.Button button_ReDefaultSettings;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown_Offset;
        private System.Windows.Forms.Label label14;
    }
}

